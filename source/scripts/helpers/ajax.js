class AjaxHelper {
	constructor() {
		this.$window = $(window);
		this.init();
	};

	change(event, {url, type = 'GET', dataType = 'JSON', data, callback, context = false}) {
		$.ajax({url, type, data, dataType,
			success: data => {
				if (typeof callback === 'function') {
					callback({data, context});
				}
			}
		});
	};

	init() {
		this.$window
			.on('ajax:get', this.change);
	};
}

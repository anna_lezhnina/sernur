class Select {
	constructor() {
		this.selectors = {
			select: '.js-select',
			native: '.js-select-native',
			sub: '.js-select-sub',
			list: '.js-select-list',
			option: '.js-select-option',
			text: '.js-select-text'
		};

		this.classes = {
			open: 'select_open',
			selected: 'select__option_selected'
		};

		this.el = $(this.selectors.select);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors)
				.init();
		}
	}

	get({native, sub, list, option, text}) {
		this.elems = this.el.toArray().map((el) => {
			const $el = $(el);
			return {
				$el,
				$native: $el.find(native),
				$sub: $el.find(sub),
				$list: $el.find(list),
				$options: $el.find(option),
				$text: $el.find(text)
			}
		});
		return this;
	}

	init() {
		this.elems.forEach(elem => {
			elem.$sub.on('click', {elem, that: this}, this.toggle);
			elem.$options.on('click', {elem, that: this}, this.select);
		});
	}

	toggle(e) {
		const {elem, that} = e.data;
		const open = e.data.open || !elem.$el.hasClass(that.classes.open);

		elem.$el.toggleClass(that.classes.open, open);

		if (open) {
			$('body').on('click', {elem, that}, that.checkCloseClick);
		}
		else {
			$('body').off('click', that.checkCloseClick);
		}
	}

	select(e) {
		const {elem, that} = e.data;
		const $selected = $(e.currentTarget);
		const value = $selected.data('value');
		const id = parseInt($selected.data('id'));

		elem.$text.text($selected.text());
		elem.$native.val(value);
		that.setSelected(elem.$options, id, that.classes.selected);
		that.toggle({
			data: {
				elem,
				that,
				open: false
			}
		});
	}

	checkCloseClick(e) {
		const	{elem, that} = e.data,
				notSelect = !$(e.target).is('.select__option, .select__text, .select__icon, .select__wrap');

		if (notSelect) {
			that.toggle({data: {elem, that}});
		}
	}

	setSelected($options, id, className) {
		$options.each((i, el) => {
			$(el).toggleClass(className, id === i);
		});
	}
}

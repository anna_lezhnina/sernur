class SubscribeOverlay {
	constructor() {
		this.selectors = {
            el: '.js-subscribe-overlay',
            form: '.js-form',
            container: '.js-container',
		};

		this.classes = {
			open: 'subscribe-overlay_open',
		};

        this.data = {
            cc: 'cc',
            msg: 'msg',
		};

        this.$window = $(window);
		this.el = $(this.selectors.el);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data)
				.init();
		}
	}

	get({form, container}, {cc, msg}) {
		this.elems = this.el.toArray().map((el) => {
            const $el = $(el);
			return {
				$el,
                $form: $el.find(form),
                $container: $el.find(container),
                data: {
                    cc: $el.data(cc),
                    msg: $el.data(msg),
                }	
			};
		});
		return this;
	}

	init() {
		this.elems.forEach(elem => {
			elem.$el.on('click', {elem, that: this}, this.toggle);
            elem.$form.on('submit', {elem, that: this}, this.subscribe);
		});
	}

	toggle(e, isForce) {
		const {elem, that} = e.data;
        const open = e.data.open || !elem.$el.hasClass(that.classes.open);
        if(($(e.target).is(that.selectors.el)) || isForce) {
            elem.$el.toggleClass(that.classes.open, open);
        };
    }
    
    subscribe(e) {
        const {elem, that} = e.data;
        e.preventDefault();
        const formData = $(this).serialize() + '&' + $.param(that.el.data());

        that.$window.trigger('ajax:get', {type: 'POST', url: '/netcat/modules/default/actions/item_arrived_notifier.php', data: formData});

		that.toggle({
			data: {
				elem,
				that,
				open: false
            }
		}, true);
    }
}

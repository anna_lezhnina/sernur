$(document).ready(function () {
    $('.recipe-page__info-content-item').on('click', function () {
        $('.recipe-page__popup').removeClass('-shown');

        var $popup = $(this).parent().find('.recipe-page__popup');
        if (!$popup) return;

        if (!$popup.hasClass('-shown')) {
            $popup.addClass('-shown');
        }

        var closeButton = $popup.find('.recipe-page__popup-close-button');
        closeButton.on('click', function () {
            if ($popup.hasClass('-shown')) {
                $popup.removeClass('-shown');
            }
        })
    });
});
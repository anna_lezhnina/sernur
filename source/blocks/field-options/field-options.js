class FieldOptions {
	constructor() {
		this.selectors = {
			elem: '.js-field-options',
			option: '.js-option',
			field: '.js-field'
		};

		this.classes = {
		};

		this.data = {
			eventChange: 'event-change'
		};

		this.el = $(this.selectors.elem);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors,  this.data)
				.init();
		}
	}

	get({option, field}, {eventChange}) {
		this.elems = this.el.toArray().map((el) => {
			const $el = $(el);

			return {
				$el,
				$options: $el.find(option),
				$field: $el.find(field),
				data: {
					change: $el.data(eventChange) || 'field-option:change'
				}
			}
		});
		return this;
	}

	init() {
		this.elems.forEach(el => {
			el.$options.on('click', {el, that: this}, this.optionClick);
			el.$el.on('field-option:validate', {el, that: this}, this.validate);
		});
	}

	validate(e) {
		const 	{el, that} = e.data;

		el.$field.trigger('field:validate');
	}

	optionClick(e) {
		const 	{el, that} = e.data,
				$cur = $(e.currentTarget),
				value = $cur.text(),
				data = $cur.data();

		el.$field
			.trigger('field:update', {value});
			// .trigger('field:focus');
		el.$el.trigger(el.data.change, {value, data});
	}
}

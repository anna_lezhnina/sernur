'use strict';

const gulp = require('gulp'),
			path = require('path'),
			fs = require('fs'),
			del = require('del'),
			rename = require('gulp-rename'),
			log = require('fancy-log'),
			beeper = require('beeper'),
			plumber = require('gulp-plumber'),
			portfinder = require('portfinder'),
			postcss = require('gulp-postcss'),
			stylus = require('gulp-stylus'),
			browserSync = require('browser-sync'),
			uglify = require('gulp-uglify'),
			concat = require('gulp-concat'),
			pug = require('gulp-pug'),
			data = require('gulp-data'),
			cache = require('gulp-cached'),
			image = require('gulp-imagemin'),
			cachebust = require('gulp-cache-bust'),
			eslint = require('gulp-eslint'),
			babel = require("gulp-babel"),
			duration = require('gulp-duration'),
			cssfont64 = require('gulp-cssfont64'),
			runSequence = require('run-sequence'),
			clean = require('gulp-clean'),
			svgmin = require('gulp-svgmin'),
			flatmap = require('gulp-flatmap'),
			inject = require('gulp-inject'),
			autoprefixer = require('autoprefixer-stylus'),
			empty = require('gulp-empty'),
			gulpIf = require('gulp-if'),
			getData = require('jade-get-data'),
			reload = browserSync.reload;

const processors = [
	require('postcss-inline-svg'),
	require('css-mqpacker'),
];

const dataPug = {
	getData: getData('source/data'),
	jv0: 'javascript:void(0);'
};

// Ресурсы

const paths = {
	source: {
		js: ['source/blocks/', 'source/scripts/helpers/', 'source/scripts/'],
		css: ['source/styles/', 'source/blocks/'],
		img: 'source/img/',
		svg: 'source/svg/',
		templates: ['source/templates/', 'source/blocks/'],
		fonts: 'source/fonts/',
		jq: './node_modules/jquery/dist/jquery.min.js',
		sisyphus: './node_modules/sisyphus.js/sisyphus.min.js',
		likely: {
			js: './node_modules/ilyabirman-likely/release/likely.js',
			css: './node_modules/ilyabirman-likely/release/likely.css'
		},
		sticky: './node_modules/stickyfilljs/dist/stickyfill.min.js',
		dadata: {
			js: './node_modules/suggestions-jquery/dist/js/jquery.suggestions.min.js',
			css: './node_modules/suggestions-jquery/dist/css/suggestions.min.css'
		},
		slider: {
			js: './node_modules/nouislider/distribute/nouislider.min.js',
			css: './node_modules/nouislider/distribute/nouislider.min.css'
		},
		numbered: './node_modules/input.numbered/numbered.js',
		data: 'source/data/',
		resources: 'source/resources/'
	},
	dest: {
		js: 'dist/js/',
		css: 'dist/css/',
		cssVendors: 'dist/css/vendors/',
		img: 'dist/img/',
		html: './dist',
		resources: 'dist/resources/'
	}
};

// Stylus params
const stylConf = {
	compress: false
}

// Inject params
const injectParams = [
	{
		name: 'svg',
		source: {
			fn: (src) => getSvg(src),
			src: paths.source.svg + '*.svg'
		},
		options: {
			starttag: '<!-- inject:svg:inline -->',
			endtag: '<!-- endinject -->',
			transform: (filePath, file) => {
				let name = filePath.substring(filePath.lastIndexOf('/')+1).split('.')[0];
				let content = file.contents.toString('utf8').replace('<svg', `<svg id="${ name }" class="svg-source"`)
					.replace('opacity=".5"', 'fill="currentColor"');
				return content;
			}
		}
	},
	{
		name: 'allJs',
		source: { 
			src: [
				paths.dest.js + '*.js',
				'!' + paths.dest.js + 'jquery.min.js',
				'!' + paths.dest.js + 'jquery.suggestions.min.js',
				'!' + paths.dest.js + 'likely.js',
				'!' + paths.dest.js + 'sisyphus.min.js',
				'!' + paths.dest.js + 'nouislider.min.js',
				'!' + paths.dest.js + 'stickyfill.min.js',
				'!' + paths.dest.js + 'numbered.js',
				'!' + paths.dest.js + 'legacy.js'
			]
		},
		options: {
			starttag: '<!-- inject:js:all -->',
			endtag: '<!-- endinject -->',
			ignorePath: '/dist',
			addRootSlash: false
		}
	},
	{
		name: 'jsJq',
		source: {src: ['jquery.min.js', 'jquery.suggestions.min.js', 'likely.js', 'nouislider.min.js', 'stickyfill.min.js', 'numbered.js', 'sisyphus.min.js'].map((val) => paths.dest.js + val)},
		options: {
			starttag: '<!-- inject:js:jq -->',
			endtag: '<!-- endinject -->',
			ignorePath: '/dist',
			addRootSlash: false
		}
	}
];

let conf = {
	pug: {
		pretty: true
	},
	stylus: {
		compress:  false
	},
	js: {
		uglify: false
	}
}

// Одноразовая сборка
gulp.task('default', () => {
	conf.pug.pretty = true;
	conf.stylus.compress = true;
	conf.js.uglify = true;

	runSequence(
		'cleanDist',
		['copyLib', 'copyData', 'copyLegacyJs'],
		['styles', 'scripts'],
		'pug',
		['img', 'cache']
	);
});

// Запуск живой сборки
gulp.task('live', () => {
	runSequence(
		//'cleanDist',
		['copyLib', 'copyData', 'copyLegacyJs'],
		['styles', 'scripts'],
		'pug',
		['img', 'cache', 'watch', 'server']
	);
});

gulp.task('no-img', () => {
    runSequence(
        ['copyLib', 'copyData', 'copyLegacyJs'],
        ['styles', 'scripts'],
        'pug',
        ['cache', 'watch', 'server']
    );
});

// Cборка с вотчем без браузерсинка
gulp.task('no-server', () => {
	runSequence(
		'cleanDist',
		['copyLib', 'copyData', 'copyLegacyJs'],
		['styles', 'scripts'],
		'pug',
		['img', 'cache', 'watch']
	);
});

// Федеральная служба по контролю за оборотом файлов
gulp.task('watch', function() {
	gulp.watch(paths.source.templates.map((el) => el + '**/*.pug'), ['pug']);
	gulp.watch(paths.source.data + '**/*.json', ['pug']);
	gulp.watch(paths.source.resources + '**/*.{json,html}', ['copyData']);
	gulp.watch(paths.source.svg + '**/*.svg', ['pug']);
	gulp.watch(paths.source.css.map((el) => el + '**/*.styl'), ['styles', 'cache']);
	gulp.watch(paths.source.css[0] + 'legacy.css', ['styles', 'cache']);
	gulp.watch(paths.source.js.map((el) => el + '**/*.js'), ['scripts', 'cache']);
	gulp.watch(paths.source.img + '*.{png,jpg,gif,svg}', ['img']).on('change', function(event) {
		if (event.type === 'deleted') {
			del(paths.bundles + path.basename(event.path));
			delete cache.caches['img'][event.path];
		}
	});
});

// Очистка dist
gulp.task('cleanDist', () => {
	return gulp.src(paths.dest.html)
		.pipe(clean({force: true}));
});


// Копирование библиотек
gulp.task('copyLib', () => {
	gulp.src([paths.source.jq, paths.source.sticky, paths.source.dadata.js, paths.source.likely.js, paths.source.sisyphus, paths.source.slider.js, paths.source.numbered])
		.pipe(gulp.dest(paths.dest.js));
	return gulp.src([paths.source.dadata.css, paths.source.likely.css, paths.source.slider.css])
		.pipe(gulp.dest(paths.dest.cssVendors));
});

// Копирование данных
gulp.task('copyData', () => {
	return gulp.src(paths.source.resources + '**/*.{json,html}')
		.pipe(gulp.dest(paths.dest.resources));
});

// Копирование legacy
gulp.task('copyLegacyJs', () => {
	return gulp.src(paths.source.js[2] + 'legacy.js')
		.pipe(gulp.dest(paths.dest.js));
});

// Шаблонизация
gulp.task('pug', function() {
	return gulp.src(paths.source.templates[0] + '*.pug')
		.pipe(plumber({errorHandler: onError}))
		.pipe(pug({pretty: conf.pug.pretty, locals: dataPug}))
		.pipe(flatmap((stream, file) => massInject(stream, file)))
		.pipe(gulp.dest(paths.dest.html))
		.pipe(reload({stream: true}));
});

// Стили
gulp.task('styles', function() {
	runSequence('stylus', 'inline-fonts', 'concat-fonts', 'clear-fonts');
});

// Сборка CSS
gulp.task('stylus', function() {
	return gulp.src(paths.source.css[0] + '*.styl')
		.pipe(stylus({
			use: [autoprefixer({browsers: ['> 1%']})],
			compress: conf.stylus.compress
		}).on('error', onError))
		.pipe(postcss(processors))
		.pipe(duration(`style.css has built`))
		.pipe(gulp.dest(paths.dest.css));
});

// Конвертация шрифтов в CSS
gulp.task('inline-fonts', function() {
	return gulp.src(paths.source.fonts + '*')
		.pipe(cssfont64())
		.pipe(gulp.dest(paths.source.fonts));
});

// Объединение основных стилей со шрифтовым CSS
gulp.task('concat-fonts', function() {
	return gulp.src([paths.source.fonts + '*.css', paths.dest.cssVendors + '*.css', paths.dest.css + 'style.css'])
		.pipe(concat('style.css'))
		.pipe(gulp.dest(paths.dest.css))
		.pipe(reload({stream: true}));
});

// Удаление шрифтового CSS
gulp.task('clear-fonts', function() {
	return gulp.src(paths.source.fonts + '*.css', {read: false})
		.pipe(clean());
});

gulp.task('copy-old-styles', () => {
	return gulp.src(paths.source.css[0] + 'legacy.css')
		.pipe(gulp.dest(paths.dest.css));
})

// Сборка и минификация скриптов
gulp.task('scripts', function() {
	return gulp.src((paths.source.js.map((el) => el + '**/*.js')))
		.pipe(plumber({errorHandler: onError}))
		// .pipe(eslint.format())
		.pipe(babel())
		.pipe(concat('scripts.js'))
		.pipe(gulpIf(conf.js.uglify, uglify(), empty()))
		.pipe(gulp.dest(paths.dest.js))
		.pipe(reload({stream: true}));
});

// Сжатие картинок
gulp.task('img', function() {
	return gulp.src(paths.source.img + '/**/*.{png,jpg,gif,svg}')
		.pipe(cache('img'))
		// .pipe(image({
		// 	verbose: true
		// }))
		.pipe(gulp.dest(paths.dest.img));
});

// Очистка кэша для скриптов и стилей
gulp.task('cache', function() {
	return gulp.src(paths.dest.html + '*.html')
		.pipe(cachebust())
		.pipe(gulp.dest(paths.dest.html))
		.pipe(reload({stream: true}));
});

// Локальный сервер
gulp.task('server', function() {
	portfinder.getPort(function(err, port) {
		browserSync({
			server: {
				baseDir: "./dist",
				serveStaticOptions: {
					extensions: ['html']
				}
			},
			host: 'localhost',
			notify: false,
			port: port,
			open: false
		});
	});
});

// Рефреш страниц
gulp.task('html', function() {
	return gulp.src(paths.dest.html + '*.html')
		.pipe(reload({stream: true}));
});

// Ошибки
const onError = function(error) {
	log([
		(error.name + ' in ' + error.plugin).bold.red,
		'',
		error.message,
		''
	].join('\n'));
	beeper();
	this.emit('end');
};

// SVG
const getSvg = (src) => {
	let source = gulp.src(src)
		.pipe(svgmin({
			plugins: [{
				removeAttrs: {
					attrs: ['fill', 'width', 'height', 'fill-rule', 'clip-rule']
				}
			}]
		}));
	return  source;
};

// Inject

const massInject = (stream, file) => {
	injectParams.forEach((el, i) => stream = injectFn(stream, el));
	return stream;
};

const injectFn = (streamTarget, conf) => {
	if (conf.source.fn === undefined || conf.source.fn === null) {
		return streamTarget
			.pipe(inject(gulp.src(conf.source.src), conf.options));
	}
	else {
		return streamTarget
			.pipe(inject(conf.source.fn(conf.source.src), conf.options));
	}
}
